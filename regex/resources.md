## Regular expressions

### What's a regular expression?

https://www.oreilly.com/library/view/introducing-regular-expressions/9781449338879/ch01.html

> “A regular expression is a pattern which specifies a set of strings of characters; it is said to match certain strings.” —Ken Thompson


### Interactive tutorial

https://regexone.com/


### Regex in Python

https://stackabuse.com/introduction-to-regular-expressions-in-python/
