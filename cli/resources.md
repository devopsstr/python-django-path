### CLI / Linux / OS basics resources

## Part 1

[Command Line Crash Course](https://learnpythonthehardway.org/book/appendixa.html)

[Commonly Used Linux Commands](https://www.thegeekstuff.com/2010/11/50-linux-commands/)

________

## Part 2

#### Unix basics

* https://neilkakkar.com/unix.html
* https://en.wikipedia.org/wiki/Unix_philosophy
* https://ph7spot.com/musings/in-unix-everything-is-a-file

#### Linux directory structure

* http://web.archive.org/web/20190722200507/https://www.thegeekstuff.com/2010/09/linux-file-system-structure/
* https://www.howtogeek.com/117435/htg-explains-the-linux-directory-structure-explained/

#### Processes

* https://en.wikipedia.org/wiki/Process_(computing)
* http://web.archive.org/web/20180214023435/https://www.thegeekstuff.com/2011/04/ps-command-examples/

#### Inter-process communication, TCP/IP

* https://en.wikipedia.org/wiki/Inter-process_communication
* https://jvns.ca/networking-zine.pdf
* https://realpython.com/python-sockets


#### apt

* https://askubuntu.com/questions/540937/what-does-apt-get-install-do-under-the-hood
