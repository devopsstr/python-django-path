## Sessions:


* Google for you topic, refer various resources, refer the official Python documetnation and learn it in depth. Also provide examples.

* Show code samples.  

_____________

## Virtualenvs, pip, pypi

* How to create virtualenvs
* How to maintain multiple virtualenvs
* Installing and uninstalling packages from pip
* Where are third party packages located? How to insert breakpoints in third-party packages
* What is pypi?

Drills:

1. Create a virtualenv by specifying the Python version
2. Create a requirements.txt
3. Add `requests` version `2.21.0` to requirements.txt
4. Add `django` version `2.1.5` to requirements.txt
5. pip install all the items in requirements.txt (in one command)
6. Upgrade django to its latest version, `2.1.7`
7. Where is the source code of `requests` and `django` located?
8. Where is the virtualenv's `python` located?
9. Deactivate the virtualenv

______________

## Python workflow using Ipython

* Everything we discussed in the ipython session... 

____________

## Defining Variable number of arguments in Python: (*args, **kwargs)

* https://www.programiz.com/python-programming/args-and-kwargs
* https://www.agiliq.com/blog/2012/06/understanding-args-and-kwargs/
* https://www.geeksforgeeks.org/packing-and-unpacking-arguments-in-python/

Look for more resources.

Checklist:

* How to define a function with variable number of arguments.
* When is it used?
* What are `*args`, `**kwargs`
* How to call a function with variable number of arguments.
* Packing and unpacking arguments (See the geeksforgeeks article)
* Examples


Drills:

* Format the string `s` by unpacking the `person` dictionary.

```
person = {'name': 'Tom', 'age': 10}

s = "I am {}. I'm {} years old."
```

* Define a function `add3`

```
def add3(a, b, c):
    return a + b + c
```

Create a list,

```
items = [1, 2, 3]
```

Call `add3` by passing `items` as the argument (Using unpacking)

Create a dictionary 

```
add_args = {'a': 1, 'b': 2, 'c': 3}
```

Call `add3` by passing `add_args` as an argument. (Using unpacking)

```
a = 1
b = 2
c = 3
```

Call `add3` by passing `a`, `b` and `c` as keyword arguments.


* Define a function `add_n` which accepts any number of arguments and returns the sum of all the arguments.

Create the following variables

```
def add_n(*args):
    # Your logic here
    pass
```

What is the type of `*args`?

* Define a function that accepts any kind of arguments and keyword-arguments, and prints the arguments and keyword-arguments.

_________

## Decorators:

* Example of a function that accepts another function as an argument
* What is a decorator? How to define a decorator?
* When do we use decorators?
* Write a decorator that logs all the arguments received by a function, and prints how much time it took to execute that function


___________

## OOP in Python

* What's a class?
* What's an object.
* What is self?
* What does __init__ do?
* What are static methods, class methods, instance methods?
* What are bound methods?
* Inheritance in Python?
* Inheritance / Composition?

Give examples

___________

Sample resource - https://www.python-course.eu/python3_magic_methods.php

(Also refer to other resources)

* What's a generator (Explain the `yield` statement)
* Give examples, both your own, and those provided by the standard library.
* What are magic methods?
* Example common magic methods like `__len__`, `__add__`
* Also build a custom class called Person as below:
    ```
    class Person:
        def __init__(self, age):
            self.age
    ```
    And then use magic methods such as `__lt__` etc to compare persons by their age.

___________

(Saturday, 13th April)

* How do relational databases store data?
* What are indexes
* What are the pros and cons?
* Provide examples
* Also improve speed of a query by adding an index
* What are transactions? Provide examples where transactions are used to maintain application-level integrity of the data.

________


(Thursday, April 2, 2019)

* https://dbader.org/blog/writing-pythonic-code
* https://medium.com/the-andela-way/idiomatic-python-coding-the-smart-way-cc560fa5f1d6
* https://docs.python-guide.org/writing/style/
* https://en.wikipedia.org/wiki/Zen_of_Python
* https://docs.python-guide.org/writing/style/#zen-of-python

Explain the Zen of Python. Show examples good / bad from the above resources. Also refer other resources if necessary


_____________

## PEP8, flake8

* https://realpython.com/python-pep8/

Checklist:

* Provide examples. 
* Run your webscraper project through flake8, and fix the errors. Show the "before" and "after" using `git diff`

___________

## context managers

* https://jeffknupp.com/blog/2016/03/07/python-with-context-managers/
* https://dbader.org/blog/python-context-managers-and-with-statement

Checklist:

* What are context managers?
* Why are they used?
* Give builtin examples, such as opening files
* Build a custom context manager
* Provide examples.

_______

## Redis

* What is Redis?
* Why is it used?
* Explain a few common commands
* Briefly explain the time-complexity of each command

Commands to be covered: (Use `redis-py` and `redis-cli`)

Basic key value stuff

* set
* get
* incr
* incrby

Sets

* sadd
* sismember
* smembers
* sunion
* sinter
* sdiff

Lists

* lpush
* lpop
* rpush
* rpop

______________

## Elasticsearch

* How do full-text search engines work? What is an inverted index?
* What is Lucene?
* What is sharding in databases and search engines?
* How does ES(Elasticsearch) use Lucene?
* Provide an overview of ES's architecture.
* Show a few example queries - (Show both matching and filtering)


## Django forms and validators

* Why are forms used?
* How to create our own forms? Provide an example
* What are Model forms? Provide an example
* How to create forms that save data into multiple models? Provide an example

## Django ORM

* What is an ORM?
* How to filter data
* How to join data from multiple tables? 
* What is the n + 1 query problem? How to avoid it in Django?
* How to calculate sum, average etc? 
* What is a queryset. How is it different from a list?
* What is a Model Manager? How to create a custom model manager. When are they used?

Provide examples for all the above.
____________

## Python overview and internals

High level overview:
    1. Dynamically typed
    2. Interpreted
    3. Bytecode - dis module
    4. CPython - ceval.c
    5. How is Python made portable?.

Interpreter:

* http://akaptur.com/blog/2013/11/15/introduction-to-the-python-interpreter/
* http://akaptur.com/blog/2013/11/15/introduction-to-the-python-interpreter-2/
* http://akaptur.com/blog/2013/11/17/introduction-to-the-python-interpreter-3/
* http://akaptur.com/blog/2013/12/03/introduction-to-the-python-interpreter-4/

GIL:

* https://wiki.python.org/moin/GlobalInterpreterLock
* https://stackoverflow.com/questions/1294382/what-is-the-global-interpreter-lock-gil-in-cpython
* https://www.youtube.com/watch?v=7SSYhuk5hmc

## Next topics:

* Multiprocessing
* TDD - Unit Testing, Integration Testing, Code Coverage (Split into 2 sessions)
* DOM, Event Loop
* Async programming in JS
* git pull request workflow
* Django in depth

## General Sessions

* Mental model

* Debugging:
    1. Testing components
    2. Interaction between components
    3. Trying different paths of inquiry
    4. Celebrating breakthroughs. 
    5. Refering tutorials
    6. Rubber duck debugging


## Based on Customer feedback

1. Time estimation session
2. Focus on debugging skills - logging, how to check logs, breakpoints etc.
3. Documentation session
4. Testing - Overview of testing - unit, integration, functional, focus more on unit testing for projects
5. Time complexity session
6. Writing clean code - SOLID principles
