## Prereqs:

1. Django tutorial
2. Django HTTP request/response lifecycle
3. Django Forms, Model Forms - Write your own examples
4. Django Class Based Views, Generic Views - Write your own examples

# Add the following features:

## qa

### Edit a Question

* Only the asker can edit a question
* Write tests with following cases:
    1. Owner edits question
    2. Another user tries to edit question

_______

### Edit an answer

* Only the answerer can edit answer

________

### Delete an answer

* Only the answerer can delete answer (Send Not Authorized status code if user does not have permission)
* Add tests (Optional)
    1. Original user deletes answer
    2. Another user tries to delete answer

________

## Article

### Optional image

Right now, each article requires an image. Change this into an optional feature.


_______

## Common Checklist:

* Work on feature branches, named  after the feature (such as `question-edit`)
* Add tests, if mentioned in the task
* Run flake8 and coverage.py


**Authorization** - https://stackoverflow.com/a/10360797/817277

Edit Question, Edit Answer, Delete Answer - Ensure the user has permissions to make changes to the selected object.

**flake8**

Run `flake8` to ensure there are no syntax errors, unused imports, long lines etc.

**AnswerForm - Create a base answer form.**

Create a `base_answer_form.html` and extend the form to create answers and edit answers from `base_answer_form.html` (Follow the DRY principle)


**Delete Answer**

Instead of `MyModel.objects.get(pk=pk)` directly, use `get_object_or_404`

**Test cases**

Query the database to ensure the operation has succeeded. Don't just check the status code.

______________

## Download user's content using a celery worker

Add a feature to download all the user's question into a JSON file. 

Since this is a computationally intensive task, it must be performed in the background. 

### Workflow:

1. Create a `url` called `qa/downloads/questions`
2. When user visits this url, show a button called "Download Your Questions"
3. When user clicks on this button, make an ajax POST request
4. On the server, start an async task to download the user's questions
5. In the background, run a celery worker that accepts the task and stores the data in a file.
6. On the browser, periodically check whether the download task is completed by polling `/qa/downloads/questions/<user_id>/status` using jQuery
7. If the task is completed, trigger the download using JavaScript
8. On the server, when you receive a request to `/qa/downloads/questions/<user_id>.json`, respond with the json file data.


