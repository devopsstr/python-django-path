## Learn Django

1. Do the Django girls tutorial - https://tutorial.djangogirls.org/en/

(Skip the PythonAnywhere parts)

__________

Now, add your own feature to the project.

* Add tags to Posts - A post can have one or more tags associated with it. Allow users to add tags when creating a post. Similarly, allow users to edit tags associated with an existing post.
* Filter posts by tag - Add a url `/posts/tagged/<tag_name>` where users can view posts belonging to a single tag. 

__________
